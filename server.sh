#!/bin/bash

# Declare constants
readonly PROJECT=$(<PROJECT)
readonly SERVER=$PROJECT-server

# Connect to PostgreSQL container
docker exec -it "$SERVER" psql -d test -U postgres
