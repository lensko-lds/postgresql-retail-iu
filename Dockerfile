FROM php:7.4.28-cli
RUN apt-get update && apt-get install -y libpq-dev && docker-php-ext-install pdo_pgsql
WORKDIR /usr/src/script
COPY PASSWORD PORT product.csv store.csv main.php ./
CMD [ "php", "./main.php" ]
