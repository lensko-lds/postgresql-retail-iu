<?php
function read_csv($file) {

    $data = [];
    $handle = fopen($file, 'r');
    $keys = fgetcsv($handle, 1000, ',');
    while (($values = fgetcsv($handle, 1000, ',')) !== false) $data[] = array_combine($keys, $values);
    fclose($handle);
    return $data;
}

function connect() {

    $password = trim(file_get_contents('PASSWORD'));
    $port = trim(file_get_contents('PORT'));
    $connection = new PDO("pgsql:host=host.docker.internal;port=$port;dbname=test", 'postgres', $password);
    $connection->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
    return $connection;
}

function create_tables($connection) {

    $connection->exec("
    CREATE TABLE product
    (
        product_id SERIAL,
        category VARCHAR(100),
        brand VARCHAR(100),
        name VARCHAR(100) UNIQUE,
        cost INT,
        price INT,
        PRIMARY KEY (product_id)
    )
    ");

    $connection->exec("
    CREATE TABLE store
    (
        store_id SERIAL,
        channel VARCHAR(100),
        city VARCHAR(100),
        name VARCHAR(100) UNIQUE,
        activity INT,
        PRIMARY KEY (store_id)
    )
    ");

    $connection->exec("
    CREATE TABLE \"order\"
    (
        order_id SERIAL,
        order_date DATE,
        status VARCHAR(100),
        store_id INT,
        create_time TIMESTAMP,
        submit_time TIMESTAMP,
        finish_time TIMESTAMP,
        update_time TIMESTAMP,
        PRIMARY KEY (order_id),
        FOREIGN KEY (store_id) REFERENCES store(store_id)
    )
    ");

    $connection->exec("
    CREATE TABLE orderline
    (
        order_id INT,
        orderline_id INT,
        product_id INT,
        create_time TIMESTAMP,
        PRIMARY KEY (order_id, orderline_id),
        FOREIGN KEY (order_id) REFERENCES \"order\"(order_id),
        FOREIGN KEY (product_id) REFERENCES product(product_id)
    )
    ");
}

function load_products($connection) {

    $data = read_csv('product.csv');
    $query = "INSERT INTO product (category, brand, name, cost, price) VALUES (:category, :brand, :name, :cost, :price)";
    $connection->beginTransaction();
    $statement = $connection->prepare($query);
    foreach ($data as $row) $statement->execute($row);
    $connection->commit();
}

function load_stores($connection) {

    $data = read_csv('store.csv');
    $query = "INSERT INTO store (channel, city, name, activity) VALUES (:channel, :city, :name, :activity)";
    $connection->beginTransaction();
    $statement = $connection->prepare($query);
    foreach ($data as $row) $statement->execute($row);
    $connection->commit();
}

function create_orders($connection) {

    $connection->exec("
    INSERT INTO \"order\" (store_id, status, create_time, update_time, order_date)
    SELECT store_id, 'Created' AS status, CURRENT_TIMESTAMP AS create_time, CURRENT_TIMESTAMP AS update_time, CURRENT_DATE AS order_date
    FROM store
    WHERE RANDOM() >= (100.0 - activity) / 100
    ");
}

function create_orderlines($connection) {

    $connection->exec("
    INSERT INTO orderline (order_id, orderline_id, product_id, create_time)

    WITH t1 AS
    (
        SELECT order_id, FLOOR(1 + RANDOM() * 3) AS orderline_count
        FROM \"order\"
        WHERE status = 'Created'
    ),

    t2 AS
    (
        SELECT order_id, 1 AS orderline_id, orderline_count FROM t1 UNION ALL
        SELECT order_id, 2 AS orderline_id, orderline_count FROM t1 UNION ALL
        SELECT order_id, 3 AS orderline_id, orderline_count FROM t1
    )

    SELECT order_id, orderline_id,
    FLOOR(1 + RANDOM() * (SELECT MAX(product_id) FROM product)) AS product_id,
    CURRENT_TIMESTAMP AS create_time
    FROM t2
    WHERE orderline_id <= orderline_count
    ");
}

function submit_orders($connection) {

    $connection->exec("
    UPDATE \"order\"
    SET status = 'Submitted', submit_time = CURRENT_TIMESTAMP, update_time = CURRENT_TIMESTAMP
    WHERE status = 'Created'
    ");
}

function process_orders($connection) {

    $connection->exec("
    UPDATE \"order\" SET
    status = CASE WHEN store_id IN (1,2) THEN 'Packed' ELSE 'Sold' END,
    finish_time = CASE WHEN store_id IN (1,2) THEN NULL ELSE CURRENT_TIMESTAMP END,
    update_time = CURRENT_TIMESTAMP
    WHERE status = 'Submitted'
    ");
}

function ship_orders($connection) {

    $connection->exec("
    UPDATE \"order\"
    SET status = 'Shipped', finish_time = CURRENT_TIMESTAMP, update_time = CURRENT_TIMESTAMP
    WHERE status = 'Packed'
    ");
}

$connection = connect();
create_tables($connection);
load_products($connection);
load_stores($connection);

while (true)
{
    ship_orders($connection);
    process_orders($connection);
    create_orders($connection);
    sleep(rand(1,4));
    create_orderlines($connection);
    sleep(rand(1,6));
    submit_orders($connection);
    sleep(rand(15,20));
    $i = isset($i) ? $i+1 : 1;
    echo "$i\n";
}
?>
