#!/bin/bash

# Generate password
if [ ! -f PASSWORD ]; then
    (echo -n Pw1_; md5sum /var/lib/dbus/machine-id | cut -f 1 -d " ") > PASSWORD
fi

# Declare constants
readonly PASSWORD=$(<PASSWORD)
readonly PORT=$(<PORT)
readonly PROJECT=$(<PROJECT)
readonly CLIENT=$PROJECT-client
readonly SERVER=$PROJECT-server

# Stop and remove containers
./stop.sh

# Start PostgreSQL container
docker run -d --rm --name="$SERVER" -p "$PORT":5432 -e "POSTGRES_PASSWORD=$PASSWORD" -e "POSTGRES_DB=test" postgres:14.2
until docker exec $SERVER pg_isready ; do sleep 1 ; done

# Build PHP image and Start PHP container
docker build -t "$CLIENT" .
docker rmi $(docker images -f dangling=true -q)
docker run -dt --rm --name "$CLIENT" --add-host host.docker.internal:host-gateway "$CLIENT"

# List containers
./status.sh
