# PostgreSQL Retail IU

## Description
The purpose of this project is to provide a live data source that can be used for data engineering, data warehouse and etl development.  

The provided data source uses two Docker containers containing a PostgreSQL server and a PHP client script to simulate the order system of an electronics retail organization with both digital and physical stores.  

When the PostgreSQL server container is running it will expose a port on the host so outside connections can reach the data source.  

The client script uses four database tables and continuously performs insert and update operations on two of them while the other two are used for static reference data.

|Table|Reference|Insert|Update|
|-----|---------|------|------|
|order||X|X|
|orderline||X||
|product|X|||
|store|X|||

Note that digital and physical orders have a slightly different progression of order statuses.  
Digital store: Created -> Submitted -> Packed -> Sent   
Physical store: Created -> Submitted -> Sold  

Also note that the order table has an update_time column that records the time of the last change so the whole table does not need to be read again to perform change data capture.  

And as a final note, this project was developed and tested on Debian 11 using the docker.io package.

## Installation
Download the project into its own directory on a linux host system with docker installed.  

By default a password will be generated when running the start.sh script, but if want to provide your own password or the path /var/lib/dbus/machine-id does not exist on your host system, you will have to first create your own PASSWORD file.  
`echo "mypassword" > PASSWORD`  

If you want to change the port of the PostgreSQL server container from 44501 to something else, you can do so by entering your preferred port number into the PORT file before running the start.sh script.  
`echo "12345" > PORT`

## Usage
Run the start.sh script to prepare the required images and launch the containers.  
`./start.sh`  

Run the stop.sh script to stop and remove the containers.  
`./stop.sh`  

Optionally run the server.sh script to connect to the PostgreSQL server and take a look inside the database.  
`./server.sh`  

Optionally run the client.sh script to connect to the PHP client and take a look at the output sent by main.php to the console.  
`./client.sh`  

To see the status of the Docker containers related to this project you can run the status.sh script.  
`./status.sh`  

If you are doing customization by editing the configuration, the reference csv files or the logic in main.php, you can just run start.sh again directly without cleaning up the old containers and images first.  

When connecting to and reading from the PostgreSQL server container the database name is __test__ and the username is __postgres__ while the password and port can be found in the PASSWORD and PORT files.  
`cat PASSWORD`  
`cat PORT`

## Sample query
Sample query that can be pasted in for testing purposes after running the server.sh script.
```
SELECT
    t3.city AS store_city,
    COUNT(DISTINCT t1.order_id) as order_count,
    COUNT(*) AS orderline_count,
    SUM(t4.price) AS revenue_sek,
    SUM(t4.price-t4.cost) AS result_sek
FROM "order" AS t1
JOIN orderline AS t2 USING(order_id)
JOIN store AS t3 USING(store_id)
JOIN product AS t4 USING(product_id)
GROUP BY t3.city
ORDER BY COUNT(*) DESC;
```

## Project status
No major changes or fixes planned for this project but expect more live data sources to be added to https://gitlab.com/lensko-lds in the future.
