#!/bin/bash

# Declare constants
readonly PROJECT=$(<PROJECT)
readonly CLIENT=$PROJECT-client

# Connect to PHP container
docker attach "$CLIENT" --detach-keys="ctrl-c"
