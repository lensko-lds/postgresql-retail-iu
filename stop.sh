#!/bin/bash

# Declare constants
readonly PROJECT=$(<PROJECT)
readonly CLIENT=$PROJECT-client
readonly SERVER=$PROJECT-server

# Stop and remove containers
docker rm -f "$CLIENT" || true
docker rm -f "$SERVER" || true
